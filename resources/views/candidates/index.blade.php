@extends('layouts.app')
@section('content')


</br></br>
<h1 class="font-weight-bold" > List of candidate</h1>
<table class="table table-bordered table-sm">
<thead class="thead-dark">
<tr>
    <th>id</th><th>Name</th><th>Email</th><th>Created</th><th>Updeated</th><th>Edit</th><th>Delete</th>
    </tr>
    <!-- the table data -->
        @foreach($candidates as $candidate)
            <tr>
                <td>{{$candidate->id}}</td>
                <td>{{$candidate->name}}</td>
                <td>{{$candidate->email}}</td>
                <td>{{$candidate->created_at}}</td>
                <td>{{$candidate->updated_at}}</td>
                <td><a href = "{{action('CandidatesController@edit',$candidate->id)}}">Edit</a></td>
                <td>
                    <a href = "{{route('candidate.delete',$candidate->id)}}">Delete</a>
                </td>
            </tr>
            
        @endforeach   
        </table>
        @if($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{$message}}</p>
    

</div>
@endif 
@endsection
