@extends('layouts.app')

@section('content') 

@if(count($errors) > 0)

<div class = "alert alert-danger">
<ul>
@foreach($errors->all() as $error)
            <li>{{$error}}</li>
@endforeach
</ul>

@endif
</br></br></br>
        <form method = "post" action = "{{action('CandidatesController@update',$id)}}">
        @method('PATCH')
        @csrf 
        <div>
                <label  class="font-weight-bold" for = "name"><h3>Candidate Name</h3></label>
                <input type = "text" name = "name" value = {{$candidate->name}}>
            </div>
            <div>
                <label class="font-weight-bold" for = "email"><h3>Candidate Email</h3></label>
                <input type = "text" name = "email" value = {{$candidate->email}}>
            </div>
            <div>
                <input type = "submit" name = "submit" value = "Update candidate">
            </div>
        </form> 
    </body>

</html>

@endsection
