@extends('layouts.app')

@section('content')          
    <div class="p-3 mb-2 bg-primary text-white"><h2>Add New Candidate</h2></div>
        <form method = "post" action = "{{action('CandidatesController@store')}}">
        @csrf 
        <div>
            
            <label  for = "name"><h3>Candiadte Name</h1></label>
            <input  type = "text" name = "name" >
            
        </div>     
        <div>
            <label for = "email"><h3>Candiadte Email</h1></label>
            <input type = "text" name = "email">
            <small id="emailHelp" class="form-text text-muted"><h6>We'll never share your email with anyone else.</h6></small>
        </div> 
        <div>
            <input class="btn btn-secondary" type = "submit" name = "submit" value = "Create candidate">
        </div>   
        <div class="form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Rememeber me</label>
  </div>                    
        </form>    
    </body>
</html>

@endsection