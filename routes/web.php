<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('candidates', 'CandidatesController')-> middleware('auth');

Route::get('candidates/delete/{id}','CandidatesController@destroy')->name('candidate.delete');


Route::get('/hello', function (){
    return 'Hello Larevel';
}); 


Route::get('/student/{id}', function ($id  = 'No student found'){
    return 'We got student with id '.$id;
});


route::get('/users/{email}/{name?}', function($email = null,$name = 'missing name'){
    if(isset($name)){
        return view('user',compact('email','name'));
    }
    else {
        return view('user',compact('email','name'));
    }

    
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
