<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CandidatesSeeder::class);
        $now = Carbon::now()->toDateTimeString();
        DB::table('candidates')->insert([
            'name' => Str::random(),
            'email' => Str::random().'@gmail.com', 
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}



  
